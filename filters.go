package grender

import (
	"encoding/json"
	"fmt"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
	"html/template"
	"math/rand"
	"reflect"
	"strings"
	"time"
)

type Filters struct {
}

func (f Filters) MapFilters() map[string]interface{} {
	return map[string]interface{}{
		"random":              f.randomGenerator,
		"count":               strings.Count,
		"split":               strings.Split,
		"title":               cases.Title(language.Russian, cases.NoLower).String,
		"lower":               strings.ToLower,
		"totitle":             strings.ToTitle,
		"makeslice":           f.makeSlice,
		"in":                  f.mapIn,
		"andlist":             f.andList,
		"upper":               strings.ToUpper,
		"concat":              f.concat,
		"unixtime":            f.unixtimeNormal,
		"unixtimeformat":      f.unixtimeNormalFormatData,
		"unixtodata":          f.unixtimeNormalFormatData,
		"datehtmltounix":      f.hTML5DataToUnix,
		"timeUnixToDataLocal": f.timeUnixToDataLocal,
		"dataLocalToTimeUnix": f.dataLocalToTimeUnix,
		"yesno":               f.yesNo,
		"html2": func(value string) template.HTML {
			return template.HTML(fmt.Sprint(value))
		},
		"type":        f.typeIs,
		"jsonconvert": f.jSONconvert,
	}
}

// возращает тип аргумента
func (f *Filters) typeIs(value interface{}) string {
	v := reflect.ValueOf(value)
	var result string
	switch v.Kind() {
	case reflect.Bool:
		result = "bool"
	case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64:
		result = "integer"
	case reflect.Uint, reflect.Uint8, reflect.Uint32, reflect.Uint64:
		result = "unsigned integer"
	case reflect.Float32, reflect.Float64:
		result = "float"
	case reflect.String:
		result = "string"
	case reflect.Slice:
		result = "slice"
	case reflect.Map:
		result = "map"
	case reflect.Chan:
		result = "chan"
	default:
		result = "undefine type"
	}
	return result
}

// проверка на вхождение значения в слайс с соответствующим типом, то есть value по типу должен быть таким же как и stock для получения корректного результата
func (f *Filters) mapIn(value interface{}, stock interface{}) bool {
	switch value.(type) {
	case int64:
		for _, x := range stock.([]int64) {
			if x == value.(int64) {
				return true
			}
		}
	case int:
		for _, x := range stock.([]int) {
			if x == value.(int) {
				return true
			}
		}
	case string:
		for _, x := range stock.([]string) {
			if x == value.(string) {
				return true
			}
		}

	}
	return false
}

// делает строковый слайс из перечисляемых значений
func (f *Filters) makeSlice(value ...string) []string {
	return value
}

func (f *Filters) andList(listValues ...interface{}) bool {
	for _, v := range listValues {
		if v == nil {
			return false
		}
	}
	return true
}

// сравнение value и возврата заданного значения в зависимости от резульатат проверки на bool
func (f *Filters) yesNo(value bool, yes, no string) string {
	if value {
		return yes
	}
	return no
}

// timeUnix -> string
func (f *Filters) unixtimeNormal(unixtime int64) string {
	return time.Unix(unixtime, 0).String()
}

// UnixTime->HTML5Data
func (f *Filters) unixtimeNormalFormatData(unixtime int64) string {
	return time.Unix(unixtime, 0).Format("2006-01-02")
}

// convert HTML5Data->UnixTime
func (f *Filters) hTML5DataToUnix(s string) int64 {
	l := "2006-01-02"
	r, _ := time.Parse(l, s)
	return r.Unix()
}

// convert timeUnix->HTML5Datatime_local(string)
func (f *Filters) timeUnixToDataLocal(unixtime int64) string {
	tmp_result := time.Unix(unixtime, 0).Format(time.RFC3339)
	g := strings.Join(strings.SplitAfterN(tmp_result, ":", 3)[:2], "")
	return g[:len(g)-1]
}

// convert HTML5Datatime_local(string)->TimeUnix
func (f *Filters) dataLocalToTimeUnix(datatimeLocal string) int64 {
	r, _ := time.Parse(time.RFC3339, datatimeLocal+":00Z")
	return r.Unix()
}

// рандомный генератор для корректного обновления css,js в head
func (f *Filters) randomGenerator() int {
	return rand.Intn(1000)
}

// JSON конвертация
func (f *Filters) jSONconvert(obj interface{}) string {
	buf, err := json.Marshal(obj)
	if err != nil {
		fmt.Printf(err.Error())
		return ""
	}
	return string(buf)
}

// слияние строк
func (f *Filters) concat(s1, s2 string) string {
	return s2 + s1
}
