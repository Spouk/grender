//---------------------------------------------------------------------------
//  рендеринг шаблонов с поддержкой "горячей" отладки, с возможностью
//  добавления собственного функционала, доступного в шаблонах
//---------------------------------------------------------------------------

package grender

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
)

const (
	PREFIXLOGGER              = "[websoup-render] "
	ERROR_HTTPMETHODNOTACCEPT = "[RENDER] http method not allowed "
	ERROR_READTEMPLATES       = "[RENDER] %s"
	ERROR_WRITETEMPLATES      = "[RENDER] %s"
	ERROR_READ_TXTFILE        = "[RENDER] %s"
	ERROR_WRONGTEMPLATES      = "[RENDER] wrong output templates %T %v\n"
	ERROR_EXECUTETEMPLATE     = "[RENDER] wrong execute template %v\n"
	ERROR_WRONGIOWRITTER      = "[RENDER] FATAL - WRONG io.WRITTER\n"
	ERROR_JSON                = "[RENDER] ERROR JSON %s\n"
	//---------------------------------------------------------------------------
	//  CONST:HTTP-MEDIATYPES
	//---------------------------------------------------------------------------
	ApplicationJSON                  = "application/json"
	ApplicationJSONCharsetUTF8       = ApplicationJSON + "; " + CharsetUTF8
	ApplicationJavaScript            = "application/javascript"
	ApplicationJavaScriptCharsetUTF8 = ApplicationJavaScript + "; " + CharsetUTF8
	ApplicationXML                   = "application/xml"
	ApplicationXMLCharsetUTF8        = ApplicationXML + "; " + CharsetUTF8
	ApplicationForm                  = "application/x-www-form-urlencoded"
	ApplicationProtobuf              = "application/protobuf"
	ApplicationMsgpack               = "application/msgpack"
	TextHTML                         = "text/html"
	TextHTMLCharsetUTF8              = TextHTML + "; " + CharsetUTF8
	TextPlain                        = "text/plain"
	TextPlainCharsetUTF8             = TextPlain + "; " + CharsetUTF8
	MultipartForm                    = "multipart/form-data"
	//---------------------------------------------------------------------------
	//  CONST: HTTP-CHARSET
	//---------------------------------------------------------------------------
	CharsetUTF8 = "charset=utf-8"
	//---------------------------------------------------------------------------
	//  CONST:  HTTP-HEADERS
	//---------------------------------------------------------------------------
	AcceptEncoding     = "Accept-Encoding"
	Authorization      = "Authorization"
	ContentDisposition = "Content-Disposition"
	ContentEncoding    = "Content-Encoding"
	ContentLength      = "Content-Length"
	ContentType        = "Content-Type"
	Location           = "Location"
	Upgrade            = "Upgrade"
	Vary               = "Vary"
	WWWAuthenticate    = "WWW-Authenticate"
	XForwardedFor      = "X-Forwarded-For"
	XRealIP            = "X-Real-IP"
)

//---------------------------------------------------------------------------
//  список дефолтных функций, входящих в список инстанс рендера, доступных
//  в шаблонах при обработке
//---------------------------------------------------------------------------

// определение типа рендера
type (
	Render struct {
		sync.RWMutex
		Temp            *template.Template
		Filters         template.FuncMap
		Debug           bool
		Path            string
		logger          *log.Logger
		Lg              io.Writer
		DebugFatal      bool //вываливать в log.Fatal при ошибка рендера шаблонов/парсинга директории с шаблонами, по умолчанию false
		logwriterEnable bool
	}
)

// добавочный для гибкости по логированию
func NewRenderL(path string, debug bool, logger io.Writer, debugFatal bool) *Render {
	sf := &Render{}
	defer sf.catcherPanic()
	sf.Filters = template.FuncMap{}
	sf.AddFilters(Filters{}.MapFilters())
	sf.Path = path
	sf.Debug = debug
	sf.DebugFatal = debugFatal
	if _, valid := logger.(io.Writer); valid {
		sf.Lg = logger
		sf.logwriterEnable = true
	} else {
		sf.logger = log.New(os.Stdout, PREFIXLOGGER, log.Ltime|log.Ldate|log.Lshortfile)
		log.Printf(ERROR_WRONGIOWRITTER)
		sf.logwriterEnable = false
	}
	sf.ReloadTemplate()
	return sf
}

// создание нового инстанса // сохраняю в целях совместимости
func NewRender(path string, debug bool, logger *log.Logger, debugFatal bool) *Render {
	sf := &Render{}
	defer sf.catcherPanic()
	sf.Filters = template.FuncMap{}
	sf.AddFilters(Filters{}.MapFilters())
	sf.Path = path
	sf.Debug = debug
	sf.DebugFatal = debugFatal
	sf.logwriterEnable = false
	if logger != nil {
		sf.logger = logger
	} else {
		sf.logger = log.New(os.Stdout, PREFIXLOGGER, log.Ltime|log.Ldate|log.Lshortfile)
	}
	sf.ReloadTemplate()
	return sf
}

// перезагрузка дерева шаблонов
func (s *Render) ReloadTemplate() {
	defer s.catcherPanic()
	if s.Debug || s.Temp == nil {
		s.Temp = template.Must(template.New("indexstock").Funcs(s.Filters).ParseGlob(s.Path))
	}
}

// перегружает отдельный блок/шаблон для обновления данных
func (s *Render) ExecuteTemplate(name string, data interface{}, w http.ResponseWriter) {
	if err := s.Temp.ExecuteTemplate(w, name, data); err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_EXECUTETEMPLATE, err.Error())))
		} else {
			if s.logger != nil {
				s.logger.Printf(fmt.Sprintf(ERROR_EXECUTETEMPLATE, err.Error()))
			}
		}
	}
}

// показ указанного шаблона, с указанием data-контейнера, и интерфейса вывода
func (s *Render) Render(name string, data interface{}, w interface{}) (err error) {
	defer s.catcherPanic()
	if s.Debug || s.Temp == nil {
		s.ReloadTemplate()
	}
	buf := new(bytes.Buffer)
	if err = s.Temp.ExecuteTemplate(buf, name, data); err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_READTEMPLATES, err.Error())))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_READTEMPLATES, err.Error()))
		}
		if s.DebugFatal {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_READTEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_READTEMPLATES, err.Error()))
			}
			log.Fatal(err)
		}
		return
	}
	switch w.(type) {
	case http.ResponseWriter:
		resp := w.(http.ResponseWriter)
		resp.Header().Add(ContentType, TextHTMLCharsetUTF8)
		if _, err := resp.Write(s.HTMLTrims(buf.Bytes())); err != nil {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error()))
			}
			return err
		}
	case *os.File:
		if _, err = w.(*os.File).Write(buf.Bytes()); err != nil {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error()))
			}
			return err
		}
	default:
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRONGTEMPLATES, nil, nil)))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_WRONGTEMPLATES, nil, nil))
		}
		if s.DebugFatal {
			log.Fatal(err)
		}
	}
	return
}

// показ указанного шаблона, с указанием data-контейнера, и интерфейса вывода + указание http кода
func (s *Render) RenderCode(httpCode int, name string, data interface{}, w interface{}) (err error) {
	defer s.catcherPanic()
	if s.Debug || s.Temp == nil {
		s.ReloadTemplate()
	}
	buf := new(bytes.Buffer)
	if err = s.Temp.ExecuteTemplate(buf, name, data); err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_EXECUTETEMPLATE, err.Error())))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_EXECUTETEMPLATE, err.Error()))
		}
		if s.DebugFatal {
			log.Fatal(err)
		}
		return err
	}
	switch w.(type) {
	case http.ResponseWriter:
		resp := w.(http.ResponseWriter)
		resp.Header().Add(ContentType, TextHTMLCharsetUTF8)
		resp.WriteHeader(httpCode)
		if _, err := resp.Write(s.HTMLTrims(buf.Bytes())); err != nil {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error()))
			}
			return err
		}
	case *os.File:
		if _, err = w.(*os.File).Write(buf.Bytes()); err != nil {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error()))
			}
			return err
		}
	default:
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRONGTEMPLATES, nil, nil)))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_WRONGTEMPLATES, nil, nil))
		}
		if s.DebugFatal {
			log.Fatal(err)
		}
	}
	return
}

func (s *Render) RenderTxt(httpCode int, name string, w interface{}) (err error) {
	//read txt file
	file, err := os.Open(name)
	if err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_READ_TXTFILE, err.Error())))
		} else {
			s.logger.Printf(ERROR_READ_TXTFILE, err.Error())
		}

		if s.DebugFatal {
			log.Fatal(err)
		}
		return err
	}
	outFile, err := ioutil.ReadAll(file)
	if err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_READ_TXTFILE, err.Error())))
		} else {
			s.logger.Printf(ERROR_READ_TXTFILE, err.Error())
		}

		if s.DebugFatal {
			log.Fatal(err)
		}
		return err
	}
	switch w.(type) {
	case http.ResponseWriter:
		resp := w.(http.ResponseWriter)
		resp.Header().Add(ContentType, TextPlain)
		resp.WriteHeader(httpCode)
		if _, err = resp.Write(outFile); err != nil {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error()))
			}
			return err
		}
	case *os.File:
		if _, err = w.(*os.File).Write(outFile); err != nil {
			if s.logwriterEnable {
				_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error())))
			} else {
				s.logger.Printf(fmt.Sprintf(ERROR_WRITETEMPLATES, err.Error()))
			}
			return err
		}
	default:
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_WRONGTEMPLATES, nil, nil)))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_WRONGTEMPLATES, nil, nil))
		}
		if s.DebugFatal {
			log.Fatal(err)
		}
	}
	return
}

// JSON записывает json(byte format) в responseWriter
func (s *Render) JSONB(httpcode int, b []byte, w http.ResponseWriter) error {
	w.Header().Set(ContentType, ApplicationJavaScriptCharsetUTF8)
	w.WriteHeader(httpcode)
	if _, err := w.Write(b); err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_JSON, err.Error())))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_JSON, err.Error()))
		}
		return err
	}
	return nil
}

// записывает json в responseWriter
func (s *Render) JSON(code int, answer interface{}, w http.ResponseWriter) (err error) {
	b, err := json.Marshal(answer)
	if err != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_JSON, err.Error())))
		} else {
			s.logger.Printf(fmt.Sprintf(ERROR_JSON, err.Error()))
		}
		return err
	}
	return s.JSONB(code, b, w)
}

// отловка паники
func (s *Render) catcherPanic() {
	msgPanic := recover()
	if msgPanic != nil && s.logger != nil {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf(ERROR_EXECUTETEMPLATE, msgPanic)))
		} else {
			s.logger.Printf(ERROR_EXECUTETEMPLATE, msgPanic)
		}
		if s.DebugFatal {
			s.logger.Fatal(msgPanic)
		}
	}
}

// вычищает пустые строки в шаблонах при рендеринге
func (s *Render) HTMLTrims(body []byte) []byte {
	result := []string{}
	for _, line := range strings.Split(string(body), "\n") {
		if len(line) != 0 && len(strings.TrimSpace(line)) != 0 {
			result = append(result, line)
		}
	}
	return []byte(strings.Join(result, "\n"))
}

// отображение всех функций-фильтров, доступных в шаблонах
func (s *Render) ShowFiltersFuncs(out io.Writer) {
	for name, f := range s.Filters {
		if s.logwriterEnable {
			_, _ = s.Lg.Write([]byte(fmt.Sprintf("`%s`:`%v`\n", name, f)))
		} else {
			s.logger.Printf("`%s`:`%v`\n", name, f)
		}
	}
}

// добавление одного фильтра в фильтровую мапу
func (s *Render) AddUserFilter(name string, f interface{}) {
	s.Filters[name] = f
}

// добавление множества фильтров в фильтровую мапу
func (s *Render) AddFilters(stack map[string]interface{}) {
	for k, v := range stack {
		s.Filters[k] = v
	}
}
